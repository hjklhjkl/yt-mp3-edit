import click
import os
import re

from yt_mp3_edit.settings import Settings
from . import __version__


@click.command()
@click.version_option(version=__version__)
@click.option('--directory', default='.')
def main(directory):
    click.echo('Welcome to the edit tool for youtube downloaded mp3s.')
    settings = Settings(directory)
    iterate_directory_mp3s(settings)


def iterate_directory_mp3s(settings):
    mp3_files = [file for file in os.listdir(settings.directory) if file.endswith('.mp3')]
    settings.digits = len(str(len(mp3_files)))
    for file in mp3_files:
        edit_mp3(file, settings)


def edit_mp3(file, settings):
    file_path = os.path.join(settings.directory, file)
    title_name = re.sub(r'-.{11}\.mp3$', '', file)
    click.echo('Renaming \'%s\':' % title_name)
    track_number = ask_track_number(settings.digits)
    mp3_name = os.path.join(settings.directory, track_number + '.' + title_name + '.mp3')
    os.rename(file_path, mp3_name)


def ask_track_number(digits):
    track_number = click.prompt('Please enter a track number', type=int)
    track_number_string = str(track_number)
    track_number_digits = len(track_number_string)
    number_of_leading_zeros = max(0, digits - track_number_digits)
    leading_zeros = ''
    for i in range(number_of_leading_zeros):
        leading_zeros += '0'
    return leading_zeros + track_number_string
