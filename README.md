# yt-mp3-edit

## Build

```bash
poetry install
```

## Run

```bash
poetry run yt-mp3-edit
```

## Program arguments

| argument | default | description |
|----------|---------|-------------|
| --directory | '.' | Path of the directory of the to be edited mp3's |
